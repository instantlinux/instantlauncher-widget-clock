export default function createWidget(widgetfolder: string) {
    // Main widget container
    var clockcontainer = document.createElement("div");
    // Style
    clockcontainer.style.position  = "absolute";
    clockcontainer.style.top       = "50%";
    clockcontainer.style.left      = "0";
    clockcontainer.style.right     = "0";
    clockcontainer.style.transform = "translate(0, -50%)";
    clockcontainer.style.textAlign = "center";

    // H3 that displays the current time
    var timetext = document.createElement("h3");
    // Style
    timetext.style.fontFamily    = "Raleway";
    timetext.style.fontSize      = "32px";
    timetext.style.color         = "#fafafa";
    timetext.style.letterSpacing = "3px";
    timetext.style.fontWeight    = "800";
    timetext.style.margin        = "0";
    timetext.style.padding       = "8px 0";

    // P that displays the current date
    var datetext = document.createElement("p");
    // Style
    datetext.style.fontFamily    = "Raleway";
    datetext.style.fontSize      = "16px";
    datetext.style.color         = "#fafafa";
    datetext.style.letterSpacing = "2px";
    datetext.style.fontWeight    = "300";
    datetext.style.margin        = "0";
    datetext.style.padding       = "8px 0";


    // Adds a zero before a number, if the number is less than 10.
    var addZeros = function(i: number) {
        var returnstring: string = i.toString();
        if (i < 10) {
            returnstring = "0" + i;
        }
        return returnstring;
    }

    // Refreshes time and date.
    var refreshclock = function() {
        console.log("Refreshing clock...")
        // Get the current time
        var currenttime = new Date();
        // Set timetext.
        var h = currenttime.getHours();
        var m = currenttime.getMinutes();
        var minutes = addZeros(m);
        timetext.innerHTML = h + ":" + minutes;
        // Set datetext.
        var date = currenttime.getDate();
        var dateZero = addZeros(date);
        var month = currenttime.getMonth() + 1;
        var monthZero = addZeros(month);
        var year = currenttime.getFullYear();
        var weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        var weekday = weekdays[currenttime.getDay()];
        datetext.innerHTML = weekday + ", " + dateZero + "." + monthZero + "." + year;
        // Refresh the clock after 40 seconds.
        setTimeout(refreshclock, 10000);
    }

    // Initialize the clock
    refreshclock();

    // Add timetext and datetext to the main container
    clockcontainer.appendChild(timetext);
    clockcontainer.appendChild(datetext);

    return clockcontainer;
}